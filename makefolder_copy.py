import os

import openpyxl
import requests

wb = openpyxl.load_workbook('Products.xlsx')

sheet = wb.active

rows = [tuple(cell.value for cell in row if cell.value is not None)
        for row in sheet]  # convert the cells to text

dirnames = list()
images = list()
text = list()

for row in rows[1:]:  # [1:], ignore column headers for better looping
    if row[0] is not None:
        # joins the Brand, Family, and Ref columns
        dirnames.append('_'.join(row[:3]))
        images.append(row[3:-2])  # stores the uris in a tuple
        text.append('\r\n'.join(row[-2:]))  # joins the last two columns

for i in range(len(dirnames)):
    if not(os.path.exists(dirnames[i])):
        os.mkdir(dirnames[i])  # create the dir
    os.chdir(dirnames[i])  # enter the dir
    print('creating', dirnames[i])
    for j, image in enumerate(images[i]):  # for all of the images
        imagereq = requests.get(image)
        imagename = 'Img{}.png'.format(j + 1)
        if imagereq.status_code == 200:  # prevents filewriting errors for bad requests
            with open(imagename, 'wb') as fp:
                fp.write(imagereq.content)
            print(' ' * 4 + 'image write successful for', imagename)
        else:
            print(' ' * 4 + 'could not download image {}, error'.format(imagename),
                  imagereq.status_code, imagereq.reason)
    with open('ProdDesc_and_Collection.txt', 'wb') as fp:
        fp.write(text[i].encode('utf8'))

    os.chdir('..')  # back out of the dir
