import os

import openpyxl
import requests

wb = openpyxl.load_workbook('Products.xlsx')

sheet = wb.active

rows = [tuple(cell.value for cell in row if cell.value is not None)
        for row in sheet]  # convert the cells to text

dirnames = list()
resources = list()


for row in rows[1:]:  # [1:], ignore column headers for better looping

    dirnames.append('_'.join(row[:1]))  # added '_'.join
    # stores the uris in a tuple
    resources.append(row[3:])


for i in range(len(dirnames)):
    if not ((i > 38) & (i < 45)):  # for testing, runs only certain rows in sheet
        continue

    if not(os.path.exists(dirnames[i])):
        os.mkdir(dirnames[i])  # create the dir
    os.chdir(dirnames[i])  # enter the dir
    print('creating', dirnames[i])
    for j, resourcefull in enumerate(resources[i]):  # for all of the images

        resourcelist = resourcefull.split('|')

        for residx, resource in enumerate(resourcelist):
            ext = '.' + resource.split('.')[-1]

            resourcereq = requests.get(resource)
            resourcename = format(dirnames[i]) + '-' + str(residx+1) + ext
            if resourcereq.status_code == 200:  # prevents filewriting errors for bad requests
                with open(resourcename, 'wb') as fp:
                    fp.write(resourcereq.content)
                print(' ' * 4 + 'resource write successful for', resourcename)
            else:
                print(' ' * 4 + 'could not download resource {}, error'.format(resourcename),
                      resourcereq.status_code, resourcereq.reason)

    os.chdir('..')  # back out of the dir
